import XCTest
@testable import HecticAnalyticsNetworking

final class HecticAnalyticsNetworkingTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual("Hello, World!", "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
