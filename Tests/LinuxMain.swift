import XCTest

import HecticAnalyticsNetworkingTests

var tests = [XCTestCaseEntry]()
tests += HecticAnalyticsNetworkingTests.allTests()
XCTMain(tests)
