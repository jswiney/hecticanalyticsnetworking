import Foundation

public struct HecticEventRequest : Codable {
    public let deviceId : String
    public let events : [HecticEvent]
    
    public init(deviceId: String, events: [HecticEvent]) {
        self.deviceId = deviceId
        self.events = events
    }
}

public struct HecticEvent: Codable {

    public let name: String
    public let device: String
    public let os: String
    public let created: TimeInterval
    public let region: String
    
    public init(name: String, device: String, os: String, created: TimeInterval, region: String) {
        self.name = name
        self.device = device
        self.os = os
        self.created = created
        self.region = region
    }

}
